#include "EnemyCharacter.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "MyProject/Gun/EnemyProjectile.h"
#include "DrawDebugHelpers.h"
#include "ConstructorHelpers.h"

AEnemyCharacter::AEnemyCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Tags.Add("Enemy");

	UCapsuleComponent* Capsule = GetCapsuleComponent();
	Capsule->InitCapsuleSize(25.0f, 100.f);

	// declare trigger capsule
	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	TriggerCapsule->InitCapsuleSize(65.0f, 100.f);
	TriggerCapsule->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	TriggerCapsule->SetupAttachment(RootComponent);
	TriggerCapsule->SetWorldScale3D(FVector(1.f));
	TriggerCapsule->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);

	// declare states
	bIsShooting = false;

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Enemy Mesh"));
	SkeletalMesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SkeletalAsset(TEXT("/Game/Core/Mannequins/Enemy.Enemy"));

	if (SkeletalAsset.Succeeded())
	{
		SkeletalMesh->SetSimulatePhysics(true);
		SkeletalMesh->SetSkeletalMesh(SkeletalAsset.Object);
		SkeletalMesh->SetRelativeLocation(FVector(0, 0, -100));
		SkeletalMesh->bCastDynamicShadow = false;
		SkeletalMesh->CastShadow = false;
		SkeletalMesh->SetWorldScale3D(FVector(1.f));
		SkeletalMesh->SetupAttachment(RootComponent);
		SkeletalMesh->SetCollisionProfileName(TEXT("Skeletal Collision"));
		SkeletalMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
		SkeletalMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	}

	//Initialize and assign HitSound
	static ConstructorHelpers::FObjectFinder<USoundCue> EnemyHitCue(TEXT("'/Game/Core/Sounds/EnemyHit.EnemyHit'"));
	static ConstructorHelpers::FObjectFinder<USoundCue> EnemyDieCue(TEXT("'/Game/Core/Sounds/EnemyDie.EnemyDie'"));
	EnemyHit = EnemyHitCue.Object;
	EnemyDie = EnemyDieCue.Object;
	EnemyAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	EnemyAudioComponent->bAutoActivate = false;
	//Attach to root so sound comes from enemy
	EnemyAudioComponent->SetupAttachment(RootComponent);
	EnemyAudioComponent->SetRelativeLocation(FVector(100.0f, 0.0f, 0.0f));
}

void AEnemyCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (EnemyHit->IsValidLowLevelFast()) {
		EnemyAudioComponent->SetSound(EnemyHit);
	}
}

// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();

	//Turns AI on
	SpawnDefaultController();
}

// Called every frame
void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!playerCharacterPointer)
	{
		playerCharacterPointer = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	}
	//if distance to player < shootrange
	else if (FVector::Distance(GetActorLocation(), playerCharacterPointer->GetActorLocation()) < TriggerShootDistance)
	{
		//rotate towards player
		FVector towardsPlayer = playerCharacterPointer->GetActorLocation() - GetActorLocation();
		FRotator towardsPlayerRotator = towardsPlayer.ToOrientationRotator();

		//When to start shooting
		if (!bIsShooting && !GetWorldTimerManager().IsTimerActive(PauseBeforeShootingTimerHandle))
		{
			float TimeTillShooting = FMath::RandRange(MinStartShootingLength, MaxStartShootingLength);
			GetWorldTimerManager().SetTimer(PauseBeforeShootingTimerHandle, this, &AEnemyCharacter::StartShooting, TimeTillShooting, false);
		}

		//shoot projectile
		if (bIsShooting && !GetWorldTimerManager().IsTimerActive(MinFireRateTimerHandle))//if cooldown between bullets is over 
		{
			ShootProjectile(towardsPlayerRotator);
			GetWorldTimerManager().SetTimer(MinFireRateTimerHandle, MinShotInterval, false);
		}

		//rotate to the right, we should adjust the enemy blueprint actually but whatever
		towardsPlayerRotator.Pitch = 0.0f;
		towardsPlayerRotator.Roll = 0.0f;
		SetActorRotation(towardsPlayerRotator);
	}
}

///<summary>Set's the enemy to shooting state</summary>
void AEnemyCharacter::StartShooting()
{
	bIsShooting = true;
	float ShootingLength = FMath::RandRange(MinShootingLength, MaxShootingLength);
	GetWorldTimerManager().SetTimer(ShootingLengthTimerHandle, this, &AEnemyCharacter::StopShooting, ShootingLength, false);
}

///<summary>Set's the enemy to idle state</summary>
void AEnemyCharacter::StopShooting()
{
	bIsShooting = false;
}

///<summary>Shoots a projectile that can damage the player</summary>
void AEnemyCharacter::ShootProjectile(FRotator towardsPlayerRotator)
{
	//Instantiate projectile
	FVector SpawnPosition = GetActorLocation() + (GetActorForwardVector() * ProjectileSpawnOffset.Y);
	SpawnPosition.Z += ProjectileSpawnOffset.Z;
	FRotator ProjectileOrientation = GetActorRightVector().ToOrientationRotator();
	towardsPlayerRotator.Yaw += FMath::RandRange(-ProjectileShotOffsetRange, ProjectileShotOffsetRange);
	FTransform ProjectileTransform = FTransform::FTransform(towardsPlayerRotator, SpawnPosition);

	AEnemyProjectile* ProjectilePointer = GetWorld()->SpawnActorDeferred<AEnemyProjectile>(PlasmaProjectile, ProjectileTransform);
	ProjectilePointer->SetDamage(ProjectileDamage);
	ProjectilePointer->SetRadius(ProjectileRadius);
	ProjectilePointer->SetVelocity(ProjectileSpeed);
	ProjectileTransform.SetRotation(FQuat::FQuat(0.0f, 0.0f, 0.0f, 0.0f));
	ProjectilePointer->FinishSpawning(ProjectileTransform);
}

// Called to bind functionality to input
void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

/// <summary>Increases enemy's current health
/// <para>Takes integer as parameter and increases the current health level.</para>
/// </summary>
void AEnemyCharacter::IncreaseHealth(int IncreaseAmount)
{
	if (CurrentHealth + IncreaseAmount >= MaxHealth)
	{
		CurrentHealth = MaxHealth;
	}

	else
	{
		CurrentHealth += IncreaseAmount;
	}
}

/// <summary>Reduce enemy's current health
/// <para>Takes integer as parameter and reduces the current health level.</para>
/// </summary>
void AEnemyCharacter::ReduceHealth(int DecreaseAmount)
{
	if (CurrentHealth > 0)
	{
		if (CurrentHealth - DecreaseAmount <= 0
			&& GetWorldTimerManager().IsTimerActive(DieTimerHandle) == false)
		{
			CurrentHealth = 0;
			EnemyAudioComponent->SetSound(EnemyDie);
			GetWorldTimerManager().SetTimer(DieTimerHandle, this, &AEnemyCharacter::Die, EnemyDie->Duration, false);
			GetRootComponent()->SetHiddenInGame(SkeletalMesh != nullptr, true);
			SetActorEnableCollision(false);
		}
		else
		{
			CurrentHealth -= DecreaseAmount;
		}
	}
	EnemyAudioComponent->Play();
}

/// <summary>Kills the enemy
///</summary>
void AEnemyCharacter::Die()
{
	Destroy();
}

void AEnemyCharacter::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

void AEnemyCharacter::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//if (OtherActor && (OtherActor != this) && OtherComp)
	//{

	//}
}

void AEnemyCharacter::SetAIController(TSubclassOf<AController> controller)
{
	AIControllerClass = controller;
}