#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Engine/Engine.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Sound/SoundCue.h"
#include "Runtime/Engine/Classes/Components/AudioComponent.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "EnemyCharacter.generated.h"

class AEnemyProjectile;

UCLASS()
class MYPROJECT_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

		// Trigger Capsule
		UPROPERTY(VisibleAnywhere, Category = "Trigger Capsule")
		class UCapsuleComponent* TriggerCapsule;

	// Skeleton Mesh 
	UPROPERTY(VisibleAnywhere, Category = "Mesh")
		class USkeletalMeshComponent* SkeletalMesh;

	// declare overlap begin function
	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// declare overlap end function
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int OtherBodyIndex);

public:
	// Sets default values for this character's properties
	AEnemyCharacter();

	virtual void PostInitializeComponents() override;

	//states
	void StartShooting();
	void StopShooting();
	bool bIsShooting;

private:
	void ShootProjectile(FRotator towardsPlayerRotator);
	FTimerHandle MinFireRateTimerHandle;
	FTimerHandle PauseBeforeShootingTimerHandle;
	FTimerHandle ShootingLengthTimerHandle;
	ACharacter* playerCharacterPointer;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		int MaxHealth = 0;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		int CurrentHealth;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		float MovementSpeed;

	UPROPERTY(EditAnywhere, Category = "Audio")
		USoundCue* EnemyHit;

	UPROPERTY(EditAnywhere, Category = "Audio")
		USoundCue* EnemyDie;

	UPROPERTY(EditAnywhere, Category = "Audio")
		UAudioComponent* EnemyAudioComponent;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		float TriggerShootDistance;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		float MinStartShootingLength;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		float MaxStartShootingLength;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		float MinShootingLength;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		float MaxShootingLength;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		float MinShotInterval;

	UPROPERTY(EditAnywhere)
		TSubclassOf<AEnemyProjectile> PlasmaProjectile;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		FVector ProjectileSpawnOffset;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		float ProjectileSpeed;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		float ProjectileDamage;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		float ProjectileRadius;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		FVector DetectionRange;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "100"))
		FVector StartingPosition;

	UPROPERTY(EditAnywhere, Category = "Enemy properties")
		bool DisplayDebugMessages;

	UPROPERTY(EditAnywhere, Category = "Enemy properties", meta = (UIMin = "0", UIMax = "20"))
		float ProjectileShotOffsetRange;
public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;


	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Health management
	void IncreaseHealth(int IncreaseAmount);
	void ReduceHealth(int DecreaseAmount);


	// Setters
	void SetMovementSpeed(float Speed) { MovementSpeed = Speed; }

	void SetDetectionRange(FVector Range) { DetectionRange = Range; }

	void SetStartingPosition(FVector Position) { StartingPosition = Position; }

	void SetAIController(TSubclassOf<AController> controller);

	// Getters
	float GetMovementSpeed() { return MovementSpeed; }

	FVector GetDetectionRange() { return DetectionRange; }

	int GetCurrentHealth() { return CurrentHealth; }

	bool GetIsShooting() { return bIsShooting; }

	// End of APawn interface
	FORCEINLINE class USkeletalMeshComponent* GetMesh() const { return SkeletalMesh; }

private:
	// Events
	void Die();

	FTimerHandle DieTimerHandle;
};