// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyTriggerCapsule.h"
#include "Engine.h"
#include "DrawDebugHelpers.h"

AEnemyTriggerCapsule::AEnemyTriggerCapsule()
{
	//Register Events
	OnActorBeginOverlap.AddDynamic(this, &AEnemyTriggerCapsule::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &AEnemyTriggerCapsule::OnOverlapEnd);
}

void AEnemyTriggerCapsule::BeginPlay()
{
	Super::BeginPlay();
	
	// Loops through every actor and finds AEnemyCharacter
	/*for (TActorIterator<AEnemyCharacter> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		// Set the pointer to actual enemy character
		Enemy = *ActorItr;
	}*/
}


/// <summary>Trigger overlap method (begin)
/// <para>This method fires when trigger begins to overlap with any actor.</para>
/// </summary>
void AEnemyTriggerCapsule::OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor)
{
	// check if Actors do not equal nullptr and that 
	if (OtherActor && OtherActor != this)
	{
		// Testing access to AEnemyCharacter-class methods
		//Enemy->ReduceHealth(10);
		//int currentHealth = Enemy->GetCurrentHealth();
	}
}

/// <summary>Trigger overlap method (end)
/// <para>This method fires when trigger ends to overlap with any actor.</para>
/// </summary>
void AEnemyTriggerCapsule::OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor)
{
	if (OtherActor && OtherActor != this)
	{

	}
}