// Fill out your copyright notice in the Description page of Project Settings.

#include "GrapplingHook.h"
#include "Engine/Engine.h"
#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
UGrapplingHook::UGrapplingHook()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	//Rope = CreateDefaultSubobject<UCableComponent>(TEXT("Rope"));
	// ...
}


// Called when the game starts
void UGrapplingHook::BeginPlay()
{
	Super::BeginPlay();

	// ...

}

void UGrapplingHook::SetPlayer(AActor* Player) {
	PlayerActor = Player;
}

void UGrapplingHook::GetRope() {
	TArray<UActorComponent*, FDefaultAllocator> ComponentArray = PlayerActor->GetComponentsByTag(TSubclassOf<UActorComponent>(), "Rope");
	//Rope = ComponentArray[0];
}

void UGrapplingHook::SwitchState(EGrapplingHookState NewHookState)
{
	CurrentState = NewHookState;
}

void UGrapplingHook::StateMachineUpdate()
{
	// TODO
	switch (CurrentState)
	{
	default:
	case idle:
		break;
	case shooting:
		HookActor->SetActorLocation(FMath::VInterpTo(HookActor->GetActorLocation(), TargetLocation, 1, RopeSpeedLaunch / (HookActor->GetActorLocation() - TargetLocation).Size()));
		if ((HookActor->GetActorLocation() - TargetLocation).Size() <= 100) {
			if (HitTarget) {
				SwitchState(attached);
			}
			else {
				Release();
			}
		}
		break;
	case attached:
		UpdateAttachedHook();
		break;
	case retracting:
		HookActor->SetActorLocation(FMath::VInterpTo(HookActor->GetActorLocation(), GetOwner()->GetActorLocation(), 1, RopeSpeedRetract / (HookActor->GetActorLocation() - GetOwner()->GetActorLocation()).Size()));
		if ((HookActor->GetActorLocation() - GetOwner()->GetActorLocation()).Size() <= DistanceToDestroyRetractedRope) {
			DestroyRope();
			SwitchState(idle);
		}
		break;
	}
}

void UGrapplingHook::DestroyRope() {
	TArray<AActor*> AttachedActors;
	HookActor->GetAttachedActors(AttachedActors);

	if (AttachedActors.Num() > 0)
	{
		TInlineComponentArray<USceneComponent*> SceneComponents;
		HookActor->GetComponents(SceneComponents);

		for (TArray< AActor* >::TConstIterator AttachedActorIt(AttachedActors); AttachedActorIt; ++AttachedActorIt)
		{
			AActor* ChildActor = *AttachedActorIt;
			if (ChildActor != NULL)
			{
				for (USceneComponent* SceneComponent : SceneComponents)
				{
					ChildActor->Destroy();
				}
			}
		}
	}

	HookActor->Destroy();
}

void UGrapplingHook::Shoot(FVector directionVector, FVector pos, UWorld *world, AActor *Player) {
	HitTarget = false;
	// Check for a hit
	FHitResult OutHit;
	// TODO: Start position to be replaced with grappling hook starting position
	FVector Start = pos;

	FVector ForwardVector = directionVector;
	FVector End = ((ForwardVector * MaxRopeLength) + Start);
	FCollisionQueryParams CollisionParams;

	//DrawDebugLine(world, Start, End, FColor::Green, true);

	// Spawn the hook and rope
	if (world)
	{
		// Create a vector based on our code
		FVector NewVec = Start;

		HookActor = GetWorld()->SpawnActor<AHook>(Hook, NewVec, FRotator::ZeroRotator);
		TargetLocation = End;
	}

	bool isHit = world->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams);

	if (isHit) {
		HitTarget = true;
		TargetLocation = OutHit.ImpactPoint;
		CurrentGrippedActor = OutHit.GetActor();
		CurrentRopeLength = (OutHit.ImpactPoint - GetOwner()->GetActorLocation()).Size();
		InitialRopeLength = CurrentRopeLength;
		RopeAnchorPoint = OutHit.ImpactPoint;

		if (CurrentGrippedActor != nullptr) {
			RopeAnchorPointLocal = OutHit.ImpactPoint - CurrentGrippedActor->GetActorLocation();
		}
	}

	SwitchState(shooting);
}

void UGrapplingHook::UpdateAttachedHook()
{
	if (CurrentRopeLength > InitialRopeLength - InitialRopePull && CurrentRopeLength > MinimalRopeLength) {
		CurrentRopeLength -= InitialRopePullStrength;
	}

	if (CurrentGrippedActor && CurrentGrippedActor->ActorHasTag(UNGRIPPABLE_TAG)) {
		Release();
	}
}

void UGrapplingHook::Release() {
	CurrentRopeLength = -1.;
	SwitchState(retracting);
}

void UGrapplingHook::ForceRelease() {
	DestroyRope();
	SwitchState(idle);
}

void UGrapplingHook::DecreaseCurrentRopeLength(float amount)
{
	CurrentRopeLength -= amount;
	if (CurrentRopeLength < MinimalRopeLength) {
		CurrentRopeLength = MinimalRopeLength;
	}
}

float UGrapplingHook::GetCurrentRopeLength()
{
	return CurrentRopeLength;
}

void UGrapplingHook::SetCurrentRopeLength(float newLength) {
	CurrentRopeLength = newLength;
}


float UGrapplingHook::GetRopeLength()
{
	return MaxRopeLength;
}

FVector UGrapplingHook::GetRopeAnchorPoint() {
	return RopeAnchorPoint;
}

FVector UGrapplingHook::GetImpactPoint() {
	return TargetLocation;
}

// Called every frame
void UGrapplingHook::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	StateMachineUpdate();
	// ...
}

/// <summary>Get the current state of grappling hook
/// <para>Returns the current state of grappling hook.</para>
/// </summary>
UGrapplingHook::EGrapplingHookState UGrapplingHook::GetCurrentState()
{
	return CurrentState;
}