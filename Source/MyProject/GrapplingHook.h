// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CollisionQueryParams.h"
#include "Camera/CameraComponent.h"
#include "Components/ActorComponent.h"
#include "MyProject/Hook.h"
#include "GrapplingHook.generated.h"



UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class MYPROJECT_API UGrapplingHook : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGrapplingHook();

	enum EGrapplingHookState { idle, shooting, attached, retracting };
	void SwitchState(EGrapplingHookState newHookState);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	EGrapplingHookState CurrentState;
	void StateMachineUpdate();
	void DestroyRope();
	void UpdateAttachedHook();
	AActor* CurrentGrippedActor;
	float CurrentRopeLength = -1.;
	FVector RopeAnchorPoint;
	FVector RopeAnchorPointLocal;
	const FName UNGRIPPABLE_TAG = "Ungrippable";

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	EGrapplingHookState GetCurrentState();

	UFUNCTION(BlueprintCallable)
		void ForceRelease();

	void SetPlayer(AActor *Player);
	void GetRope();
	void Shoot(FVector directionVector, FVector pos, UWorld *world, AActor *Player);
	void Release();
	void DecreaseCurrentRopeLength(float amount);
	float GetCurrentRopeLength();
	void SetCurrentRopeLength(float newLength);
	FVector GetRopeAnchorPoint();

	UPROPERTY(EditAnywhere)
		TSubclassOf<AHook> Hook;

	UPROPERTY(EditAnywhere)
		float MaxRopeLength;

	UPROPERTY(EditAnywhere)
		float DistanceToDestroyRetractedRope = 100;

	UPROPERTY(EditAnywhere)
		float InitialRopePull = 100;

	UPROPERTY(EditAnywhere)
		float InitialRopePullStrength = 5;

	UPROPERTY(EditAnywhere)
		float RopeSpeedLaunch;

	UPROPERTY(EditAnywhere)
		float RopeSpeedRetract;

	UPROPERTY(EditAnywhere)
		float MinimalRopeLength = 1;



	AActor* PlayerActor;
	AActor* HookActor;
	UActorComponent* Rope;
	FVector TargetLocation;
	bool HitTarget = false;

	FVector GetImpactPoint();
	float GetRopeLength();
	float InitialRopeLength;
};
