// Fill out your copyright notice in the Description page of Project Settings.

#include "Gun.h"
#include <string>
#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
UGun::UGun()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UGun::BeginPlay()
{
	Super::BeginPlay();

	bClipEmpty = false;
	bIsReloading = false;
	BulletsLeftInClip = ClipSize;
}

// Called every frame
void UGun::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

/// <summary>Adds the amount to the ammo total </summary>
/// <param name="Amount">The amount of ammo added</param>
void UGun::AddAmmo(int Amount) {
	AmmoTotal += Amount;
}

/// <summary>Starts the reload of the gun</summary>
void UGun::TryReload() {
	if (BulletsLeftInClip < ClipSize && AmmoTotal != 0)
	{
		if (bDisplayDebugMessages)
		{
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, FString::Printf(TEXT("Reload elapsed Timer: %f"), GetOwner()->GetWorldTimerManager().GetTimerElapsed(ReloadTimerHandle)));
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, FString::Printf(TEXT("Reload remaing Timer: %f"), GetOwner()->GetWorldTimerManager().GetTimerRemaining(ReloadTimerHandle)));
		}

		//Set Reload timer
		if (!GetOwner()->GetWorldTimerManager().IsTimerActive(ReloadTimerHandle))
		{
			GetOwner()->GetWorldTimerManager().SetTimer(ReloadTimerHandle, this, 
														&UGun::OnReloadComplete, ReloadLength, false);
			bIsReloading = true;
		}
	}
}

/// <summary>Fills the clip of the gun and removes corresponding amount from ammo</summary>
void UGun::OnReloadComplete() {
	bClipEmpty = false;
	bIsReloading = false;
	AmmoTotal -= ClipSize - BulletsLeftInClip;
	if (AmmoTotal < 0)
	{
		BulletsLeftInClip = -AmmoTotal;
		AmmoTotal = 0;
	}
	else 
	{
		BulletsLeftInClip = ClipSize;
	}

	if (bDisplayDebugMessages) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, FString::Printf(TEXT("Ammo left: %d"), AmmoTotal));
}

/// <summary>Starts gun shooting logic </summary>
/// <param name="ShootOrigin">The starting point of the raytrace</param>
/// <param name="ForwardVector">The direction of the raytrace</param>
void UGun::TryShoot(FVector ShootOrigin, FVector ForwardVector) {
	if (GetOwner()->GetWorldTimerManager().IsTimerActive(ReloadTimerHandle))//if reloading
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("Reloading gun, shooting not possible")));
		return;
	}

	if(!GetOwner()->GetWorldTimerManager().IsTimerActive(MinFireRateTimerHandle))//if cooldown between bullets is over 
	{
		OnShoot(ShootOrigin, ForwardVector);
		GetOwner()->GetWorldTimerManager().SetTimer(MinFireRateTimerHandle, MinShootInterval, false);

		if (BulletsLeftInClip <= 0)
		{
			bClipEmpty = true;
			TryReload();
			return;
		}
	}
}

/// <summary>Generic shooting method which removes bullets from clip </summary>
/// <param name="ShootOrigin">The starting point of the raytrace</param>
/// <param name="ForwardVector">The direction of the raytrace</param>
void UGun::OnShoot(FVector ShootOrigin, FVector ForwardVector) {
	BulletsLeftInClip--;
	
	if (bDisplayDebugMessages)
	{
		FString DebugMessage = "Bullets left in clip: ";
		DebugMessage.AppendInt(BulletsLeftInClip);
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, DebugMessage);
	}
}
