// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Engine/Engine.h"
#include "Gun.generated.h"

class AEnemyCharacter;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class MYPROJECT_API UGun : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGun();
	void TryShoot(FVector ShootOrigin, FVector ForwardVector);
	void TryReload();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gun")
		bool bIsReloading;

	void AddAmmo(int Amount);

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	int GetDamage();
	float GetReloadLength();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void OnShoot(FVector ShootOrigin, FVector ForwardVector);

	UPROPERTY(EditAnywhere)
		int ClipSize = 10;
	UPROPERTY(EditAnywhere)
		int AmmoTotal = 100;
	UPROPERTY(EditAnywhere)
		float ReloadLength = 1;
	UPROPERTY(EditAnywhere)
		float BaseDamage = 1;
	UPROPERTY(EditAnywhere)
		float MinShootInterval;

	UPROPERTY(EditAnywhere)
		bool bDisplayDebugMessages = false;

	void OnReloadComplete();
	FTimerHandle ReloadTimerHandle;
	FTimerHandle MinFireRateTimerHandle;

	bool bClipEmpty;
	int BulletsLeftInClip;
};
