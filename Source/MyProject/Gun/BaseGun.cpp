// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseGun.h"
#include <string>
#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
UBaseGun::UBaseGun()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UBaseGun::BeginPlay()
{
	Super::BeginPlay();


	bClipEmpty = false;
	bIsReloading = false;
	BulletsLeftInClip = ClipSize;
}


// Called every frame
void UBaseGun::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

/// <summary>Starts gun shooting logic which checks if the gun is able to shoot </summary>
/// <param name="ShootOrigin">The starting point of the raytrace</param>
/// <param name="ForwardVector">The direction of the raytrace</param>
void UBaseGun::TryFire_Implementation(FVector ShootOrigin, FVector ForwardVector)
{
	if (GetOwner()->GetWorldTimerManager().IsTimerActive(ReloadTimerHandle))//if reloading
	{
		return;
	}

	if (!GetOwner()->GetWorldTimerManager().IsTimerActive(MinFireRateTimerHandle))//if cooldown between bullets is over 
	{
		OnFire(ShootOrigin, ForwardVector);
		GetOwner()->GetWorldTimerManager().SetTimer(MinFireRateTimerHandle, MinShootInterval, false);

		if (BulletsLeftInClip <= 0)
		{
			bClipEmpty = true;
			TryReloading();
			return;
		}
	}
}

/// <summary>Generic shooting method which removes bullets from the clip and fires a raytrace</summary>
/// <param name="ShootOrigin">The starting point of the raytrace</param>
/// <param name="ForwardVector">The direction of the raytrace</param>
void UBaseGun::OnFire_Implementation(FVector ShootOrigin, FVector ForwardVector)
{
	BulletsLeftInClip--;

	//Define bullet raytrace properties
	FHitResult OutHit;
	FVector Start = ShootOrigin;
	FVector End = ((ForwardVector * BulletMaxTravelDistance) + Start);
	FCollisionQueryParams CollisionParams;

	/*if (DisplayDebugMessages)*/ DrawDebugLine(GetWorld(), Start, End, FColor::Red, true, 0.3f);
	//Fire Raytrace
	bool isHit = GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams);
	if (isHit)
	{
		if (OutHit.bBlockingHit)
		{
			if (OutHit.GetActor() && OutHit.GetActor()->ActorHasTag(TEXT("Enemy")))
			{
				AEnemyCharacter* HitEnemy = (AEnemyCharacter*)OutHit.GetActor();
				HitEnemy->ReduceHealth(GetDamage());
			}
		}
	}

	if (bDisplayDebugMessages)
	{
		FString DebugMessage = "Bullets left in clip: ";
		DebugMessage.AppendInt(BulletsLeftInClip);
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, DebugMessage);
	}
}

/// <summary>Checks if gun reload is possible and starts it</summary>
void UBaseGun::TryReloading_Implementation()
{
	if (BulletsLeftInClip < ClipSize && AmmoTotal != 0)
	{
		if (bDisplayDebugMessages)
		{
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, FString::Printf(TEXT("Reload elapsed Timer: %f"), GetOwner()->GetWorldTimerManager().GetTimerElapsed(ReloadTimerHandle)));
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, FString::Printf(TEXT("Reload remaing Timer: %f"), GetOwner()->GetWorldTimerManager().GetTimerRemaining(ReloadTimerHandle)));
		}

		//Set Reload timer
		if (!GetOwner()->GetWorldTimerManager().IsTimerActive(ReloadTimerHandle))
		{
			GetOwner()->GetWorldTimerManager().SetTimer(ReloadTimerHandle, this,
				&UBaseGun::OnReloadingComplete, ReloadLength, false);
			bIsReloading = true;
		}
	}
}

/// <summary>Get's the current damage output of the gun</summary>
float UBaseGun::GetDamage_Implementation()
{
	return BaseDamage;
}

/// <summary>Fills the clip of the gun and removes corresponding amount from ammo</summary>
void UBaseGun::OnReloadingComplete()
{
	bClipEmpty = false;
	bIsReloading = false;
	AmmoTotal -= ClipSize - BulletsLeftInClip;
	if (AmmoTotal < 0)
	{
		BulletsLeftInClip = -AmmoTotal;
		AmmoTotal = 0;
	}
	else
	{
		BulletsLeftInClip = ClipSize;
	}
}

