// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MyProject/EnemyCharacter.h"
#include "Gun/IGun.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Engine/Engine.h"
#include "BaseGun.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYPROJECT_API UBaseGun : public UActorComponent, public IIGun
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBaseGun();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//overriden Interface methods
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gun Interface")
		void TryFire(FVector ShootOrigin, FVector ForwardVector);
		virtual void TryFire_Implementation(FVector ShootOrigin, FVector ForwardVector) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gun Interface")
		void OnFire(FVector ShootOrigin, FVector ForwardVector);
		virtual void OnFire_Implementation(FVector ShootOrigin, FVector ForwardVector) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gun Interface")
		void TryReloading();
		virtual void TryReloading_Implementation() override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gun Interface")
		float GetDamage();
		virtual float GetDamage_Implementation() override;

	//local methods
	void OnReloadingComplete();

	//variables
	UPROPERTY(EditAnywhere)
		int ClipSize = 10;
	UPROPERTY(EditAnywhere)
		int AmmoTotal = 100;
	UPROPERTY(EditAnywhere)
		float ReloadLength = 1;
	UPROPERTY(EditAnywhere)
		float BaseDamage = 1;
	UPROPERTY(EditAnywhere)
		float MinShootInterval = 0;
	UPROPERTY(EditAnywhere)
		float BulletMaxTravelDistance = 1000;
	bool bClipEmpty;
	int BulletsLeftInClip;
	FTimerHandle ReloadTimerHandle;
	FTimerHandle MinFireRateTimerHandle;

	//Blueprint variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gun")
		bool bIsReloading;

	//testing
	UPROPERTY(EditAnywhere)
		bool bDisplayDebugMessages = true;
};
