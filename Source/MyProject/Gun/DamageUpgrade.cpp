// Fill out your copyright notice in the Description page of Project Settings.

#include "DamageUpgrade.h"

// Sets default values for this component's properties
UDamageUpgrade::UDamageUpgrade()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UDamageUpgrade::BeginPlay()
{
	Super::BeginPlay(); 
}

// Called every frame
void UDamageUpgrade::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

/// <summary>overrides the base damage output of the gun and previous upgrades</summary>
float UDamageUpgrade::GetDamage_Implementation()
{
	return BaseGunPointer->GetDamage() + UpgradeDamage;
}
