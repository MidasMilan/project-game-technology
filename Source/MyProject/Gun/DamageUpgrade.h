// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gun/GunDecorator.h"
#include "DamageUpgrade.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API UDamageUpgrade : public UGunDecorator
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UDamageUpgrade();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	float GetDamage();
	virtual float GetDamage_Implementation() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Upgrade Properties")
		float UpgradeDamage = 50.0f;
};
