// Fill out your copyright notice in the Description page of Project Settings.

#define print(text) if(GEngine) GEngine->AddOnScreenDebugMessage(-1,3, FColor::Cyan, text)
#define printf(text, fstring)  if(GEngine) GEngine->AddOnScreenDebugMessage(-1,3, FColor::Cyan, FString::Printf(TEXT(text), fstring))

#include "EnemyProjectile.h"
#include "MyProject/EnemyCharacter.h"
#include "MyProject/PlayerCharacter.h"
#include <string>
#include "DrawDebugHelpers.h"

// Sets default values
AEnemyProjectile::AEnemyProjectile(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Find assets
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(TEXT("StaticMesh'/Game/StarterContent/Props/MaterialSphere.MaterialSphere'"));
	static ConstructorHelpers::FObjectFinder<UMaterialInstance> MaterialAsset(TEXT("Material'/Game/Core/Materials/EnemyProjectileInstance.EnemyProjectileInstance'"));

	//Set Mesh and material
	MeshComponent = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("BaseMesh"));
	MeshComponent->SetWorldScale3D(FVector(1.0f));
	MeshComponent->SetStaticMesh(MeshAsset.Object);
	MeshComponent->SetMaterial(0, MaterialAsset.Object);
	MeshComponent->SetupAttachment(RootComponent);

	//Colision setup
	MeshComponent->SetCollisionProfileName("EnemyProjectile");
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AEnemyProjectile::OnOverlapBegin);
	MeshComponent->OnComponentHit.AddDynamic(this, &AEnemyProjectile::OnHit);

	//Projectile movement setup
	ProjectileMovement = ObjectInitializer.CreateDefaultSubobject<UProjectileMovementComponent>(this, TEXT("ProjectileComp"));
	ProjectileMovement->ProjectileGravityScale = 0.0f;
}

/// <summary>Sets the projectile velocity</summary>
/// <param name="Speed">The speed of the projectile</param>
void AEnemyProjectile::SetVelocity(float Speed)
{
	ProjectileMovement->InitialSpeed = Speed;
	ProjectileMovement->MaxSpeed = Speed;

	if (ProjectileMovement)
	{
		ProjectileMovement->Velocity = GetActorForwardVector() * ProjectileMovement->InitialSpeed;
	}
}

/// <summary>Sets the projectile radius</summary>
/// <param name="Radius">The radius of the spawned projectile</param>
void AEnemyProjectile::SetRadius(float Radius)
{
	MeshComponent->SetWorldScale3D(FVector(Radius / 2));
}

/// <summary>Sets the projectile damage</summary>
/// <param name="Damage">The amount of damage the projectile will do</param>
void AEnemyProjectile::SetDamage(float Damage)
{
	DamageAmount = Damage;
}

// Called when the game starts or when spawned
void AEnemyProjectile::BeginPlay()
{
	Super::BeginPlay();

	SetActorEnableCollision(true);
}

// Called every frame
void AEnemyProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AEnemyProjectile::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		UE_LOG(LogTemp, Warning, TEXT("Overlap started Enemy Projectile"));

		//Collision with enemy
		if (AEnemyCharacter* EnemyCharacter = Cast<AEnemyCharacter>(OtherActor))
		{
			EnemyCharacter->ReduceHealth(DamageAmount);
		}

		//Collision with player
		if (APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(OtherActor))
		{
			PlayerCharacter->UpdateHealth(-DamageAmount);
		}

		Destroy();
	}
	else //if not an actor, it's a mesh, aka a wall
	{
		Destroy();
	}
}

void AEnemyProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		Destroy();
	}
}

