// Fill out your copyright notice in the Description page of Project Settings.

#include "GunDecorator.h"
#include "MyProject/PlayerCharacter.h"

// Sets default values for this component's properties
UGunDecorator::UGunDecorator()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	bBaseGunNotSet = true;
}


// Called when the game starts
void UGunDecorator::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void UGunDecorator::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	if (bBaseGunNotSet)
	{
		ACharacter* myCharacter = UGameplayStatics::GetPlayerCharacter((UObject*)GetWorld(), 0);
		if (IsValid(myCharacter)) {
			APlayerCharacter* PlayerCharacterPointer = Cast<APlayerCharacter>(myCharacter);
			if (PlayerCharacterPointer)
			{
				GEngine->AddOnScreenDebugMessage(-1, 150.f, FColor::Red, FString::Printf(TEXT("Thing set!!!")));
				BaseGunPointer = PlayerCharacterPointer->GetBaseGunPointer();
				bBaseGunNotSet = false;
			}
		}
	}
}

/// <summary>virtual method to be overwritten that implements additional logic to check if the gun is fireable </summary>
/// <param name="ShootOrigin">The starting point of the raytrace</param>
/// <param name="ForwardVector">The direction of the raytrace</param>
void UGunDecorator::TryFire_Implementation(FVector ShootOrigin, FVector ForwardVector)
{
}

/// <summary>virtual method to be overwritten that implements additional gun firing logic </summary>
/// <param name="ShootOrigin">The starting point of the raytrace</param>
/// <param name="ForwardVector">The direction of the raytrace</param>
void UGunDecorator::OnFire_Implementation(FVector ShootOrigin, FVector ForwardVector)
{
}

/// <summary>virtual method to be overwritten that adds additional reloading logic </summary>
void UGunDecorator::TryReloading_Implementation()
{
}

/// <summary>virtual method to be overwritten that adds additional damage </summary>
float UGunDecorator::GetDamage_Implementation()
{
	return BaseGunPointer->GetDamage();
}

