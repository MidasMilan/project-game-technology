// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Gun/IGun.h"
#include "Gun/BaseGun.h"
#include "GunDecorator.generated.h"

class APlayerCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYPROJECT_API UGunDecorator : public UActorComponent, public IIGun
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGunDecorator();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gun Interface")
		void TryFire(FVector ShootOrigin, FVector ForwardVector);
		virtual void TryFire_Implementation(FVector ShootOrigin, FVector ForwardVector) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gun Interface")
		void OnFire(FVector ShootOrigin, FVector ForwardVector);
		virtual void OnFire_Implementation(FVector ShootOrigin, FVector ForwardVector) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gun Interface")
		void TryReloading();
		virtual void TryReloading_Implementation() override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gun Interface")
		float GetDamage();
		virtual float GetDamage_Implementation() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gun Interface")
		UBaseGun* BaseGunPointer;
		
	bool bBaseGunNotSet;
};
