// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "IGun.generated.h"

// This class does not need to be modified.
UINTERFACE(BlueprintType)
class UIGun : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MYPROJECT_API IIGun
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gun Interface")
		void TryFire(FVector ShootOrigin, FVector ForwardVector);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gun Interface")
		void OnFire(FVector ShootOrigin, FVector ForwardVector);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gun Interface")
		void TryReloading();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gun Interface")
		float GetDamage();

};
