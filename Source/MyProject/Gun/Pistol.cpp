// Fill out your copyright notice in the Description page of Project Settings.

#include "Pistol.h"
#include <string>
#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
UPistol::UPistol()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UPistol::BeginPlay()
{
	Super::BeginPlay();
}

/// <summary>Shoot's a raytrace to damage a enemy </summary>
/// <param name="ShootOrigin">The starting point of the raytrace</param>
/// <param name="ForwardVector">The direction that the raytrace is aimed at</param>
void UPistol::OnShoot(FVector ShootOrigin, FVector ForwardVector) {
	Super::OnShoot(ShootOrigin, ForwardVector);
	
	//Define bullet raytrace properties
	FHitResult OutHit;
	FVector Start = ShootOrigin;
	FVector End = ((ForwardVector * BulletMaxTravelDistance) + Start);
	FCollisionQueryParams CollisionParams;

	/*if (DisplayDebugMessages)*/ DrawDebugLine(GetWorld(), Start, End, FColor::Red, true, 0.3f);
	//Fire Raytrace
	bool isHit = GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams);
	if (isHit)
	{
		if (OutHit.bBlockingHit)
		{
			if (OutHit.GetActor() && OutHit.GetActor()->ActorHasTag(TEXT("Enemy")))
			{
				AEnemyCharacter* HitEnemy = (AEnemyCharacter*)OutHit.GetActor();
				HitEnemy->ReduceHealth(BaseDamage);
			}

			if (bDisplayDebugMessages)
			{
				GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("You are hitting: %s"), *OutHit.GetComponent()->GetName()));
				GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Purple, FString::Printf(TEXT("Impact Point: %s"), *OutHit.ImpactPoint.ToString()));
				GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, FString::Printf(TEXT("Normal Point: %s"), *OutHit.ImpactNormal.ToString()));
			}
		}
	}
}