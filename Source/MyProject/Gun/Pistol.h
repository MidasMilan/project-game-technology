// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gun.h"
#include "MyProject/EnemyCharacter.h"
#include "Pistol.generated.h"

/**
 *
 */
UCLASS()
class MYPROJECT_API UPistol : public UGun
{
	GENERATED_BODY()

private:
	UPistol();
	virtual void OnShoot(FVector ShootOrigin, FVector ForwardVector) override;

	UPROPERTY(EditAnywhere)
		float BulletMaxTravelDistance = 10000;
	
protected:
	virtual void BeginPlay() override;
};
