// Fill out your copyright notice in the Description page of Project Settings.

#include "PlasmaGun.h"

/// <summary>Shoot's a projectile in a direction</summary>
/// <param name="ShootOrigin">The spawn point of the projectile</param>
/// <param name="ForwardVector">The direction the projectile will travel</param>
void UPlasmaGun::OnShoot(FVector ShootOrigin, FVector ForwardVector) {
	Super::OnShoot(ShootOrigin, ForwardVector);

	//Instantiate projectile
	FVector SpawnPosition = ShootOrigin + (ForwardVector * ProjectileSpawnOffset.Y);
	SpawnPosition.Z += ProjectileSpawnOffset.Z;
	FRotator ProjectileOrientation = ForwardVector.ToOrientationRotator();
	FTransform ProjectileTransform = FTransform::FTransform(ProjectileOrientation, SpawnPosition);

	APlasmaGunProjectile* ProjectilePointer = GetWorld()->SpawnActorDeferred<APlasmaGunProjectile>(PlasmaProjectile, ProjectileTransform);
	ProjectilePointer->SetDamage(BaseDamage);
	ProjectilePointer->SetRadius(ProjectileRadius);
	ProjectilePointer->SetVelocity(ProjectileSpeed);
	ProjectileTransform.SetRotation(FQuat::FQuat(0.0f, 0.0f, 0.0f, 0.0f));
	ProjectilePointer->FinishSpawning(ProjectileTransform);
}
