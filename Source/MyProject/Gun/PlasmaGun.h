// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gun.h"
#include "Gun/PlasmaGunProjectile.h"
#include "PlasmaGun.generated.h"

/**
 *
 */
UCLASS()
class MYPROJECT_API UPlasmaGun : public UGun
{
	GENERATED_BODY()

public:

private:
	virtual void OnShoot(FVector ShootOrigin, FVector ForwardVector) override;

	UPROPERTY(EditAnywhere)
		TSubclassOf<APlasmaGunProjectile> PlasmaProjectile;

	UPROPERTY(EditAnywhere)
		float ProjectileSpeed = 1.0f;
	UPROPERTY(EditAnywhere)
		float ProjectileRadius = 1.0f;
	UPROPERTY(EditAnywhere)
		FVector ProjectileSpawnOffset = FVector(0.0f, 120.0f, 60.0f);
};
