// Fill out your copyright notice in the Description page of Project Settings.

#include "PlasmaGunProjectile.h"
#include "MyProject/EnemyCharacter.h"
#include "MyProject/PlayerCharacter.h"
#include <string>
#include "DrawDebugHelpers.h"

// Sets default values
APlasmaGunProjectile::APlasmaGunProjectile(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Find assets
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(TEXT("StaticMesh'/Game/StarterContent/Props/MaterialSphere.MaterialSphere'"));
	static ConstructorHelpers::FObjectFinder<UMaterialInstance> MaterialAsset(TEXT("Material'/Game/Core/Materials/PlayerProjectileInstance.PlayerProjectileInstance'"));

	//Set Mesh and material
	MeshComponent = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("BaseMesh"));
	MeshComponent->SetWorldScale3D(FVector(1.0f));
	MeshComponent->SetStaticMesh(MeshAsset.Object);
	MeshComponent->SetMaterial(0, MaterialAsset.Object);
	MeshComponent->SetupAttachment(RootComponent);

	//Colision setup
	MeshComponent->SetCollisionProfileName("PlayerProjectile");
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &APlasmaGunProjectile::OnOverlapBegin);
	MeshComponent->OnComponentHit.AddDynamic(this, &APlasmaGunProjectile::OnHit);

	//Projectile movement setup
	ProjectileMovement = ObjectInitializer.CreateDefaultSubobject<UProjectileMovementComponent>(this, TEXT("ProjectileComp"));
	ProjectileMovement->ProjectileGravityScale = 0.0f;
}

/// <summary>Sets the projectile velocity</summary>
/// <param name="Direction">The orientation in which the projectile will fly</param>
/// <param name="Speed">The flying speed of the spawned projectile</param>
void APlasmaGunProjectile::SetVelocity(float Speed)
{
	ProjectileMovement->InitialSpeed = Speed;
	ProjectileMovement->MaxSpeed = Speed;

	if (ProjectileMovement)
	{
		ProjectileMovement->Velocity = GetActorForwardVector() * ProjectileMovement->InitialSpeed;
	}
}

/// <summary>Sets the projectile radius</summary>
/// <param name="Radius">The radius of the spawned projectile</param>
void APlasmaGunProjectile::SetRadius(float Radius)
{
	MeshComponent->SetWorldScale3D(FVector(Radius / 2));
}

/// <summary>Sets the projectile damage</summary>
/// <param name="Damage">The amount of damage the projectile will do</param>
void APlasmaGunProjectile::SetDamage(float Damage)
{
	DamageAmount = Damage;
}

// Called when the game starts or when spawned
void APlasmaGunProjectile::BeginPlay()
{
	Super::BeginPlay();

	SetActorEnableCollision(true);
}

// Called every frame
void APlasmaGunProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlasmaGunProjectile::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		UE_LOG(LogTemp, Warning, TEXT("Overlap started Projectile"));

		//Collision with enemy
		if (AEnemyCharacter* EnemyCharacter = Cast<AEnemyCharacter>(OtherActor))
		{
			UE_LOG(LogTemp, Warning, TEXT("THIS IS AN ENEMY"));
			EnemyCharacter->ReduceHealth(DamageAmount);
		}

		//Collision with player
		if (APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(OtherActor))
		{
			UE_LOG(LogTemp, Warning, TEXT("THIS IS A PLAYER"));
			return;
		}

		Destroy();
	}
	else //if not an actor, it's a mesh, aka a wall
	{
		Destroy();
	}
}

void APlasmaGunProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		Destroy();
	}
}

