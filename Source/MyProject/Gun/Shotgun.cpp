// Fill out your copyright notice in the Description page of Project Settings.

#include "Shotgun.h"
#include <string>
#include "DrawDebugHelpers.h"


// Sets default values for this component's properties
UShotgun::UShotgun()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UShotgun::BeginPlay()
{
	Super::BeginPlay();
}

/// <summary>Shoot's a raytrace to damage a enemy </summary>
/// <param name="StartPoint">The starting point of the raytrace</param>
/// <para name="EndPoint">The end point of the raytrace</param>
void UShotgun::FirePellet(FVector StartPoint, FVector EndPoint) {
	FHitResult OutHit;
	FCollisionQueryParams CollisionParams;

	/*if (DisplayDebugMessages)*/ DrawDebugLine(GetWorld(), StartPoint, EndPoint, FColor::Red, true, 0.3f);
	bool isHit = GetWorld()->LineTraceSingleByChannel(OutHit, StartPoint, EndPoint, ECC_Visibility, CollisionParams);
	if (isHit)
	{
		if (OutHit.bBlockingHit)
		{
			if (OutHit.GetActor() && OutHit.GetActor()->ActorHasTag(TEXT("Enemy")))
			{
				AEnemyCharacter* HitEnemy = (AEnemyCharacter*)OutHit.GetActor();
				HitEnemy->ReduceHealth(BaseDamage);
			}

			if (bDisplayDebugMessages)
			{
				GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, FString::Printf(TEXT("You are hitting: %s"), *OutHit.GetComponent()->GetName()));
				GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Purple, FString::Printf(TEXT("Impact Point: %s"), *OutHit.ImpactPoint.ToString()));
				GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Blue, FString::Printf(TEXT("Normal Point: %s"), *OutHit.ImpactNormal.ToString()));
			}
		}
	}
}

/// <summary>Shoot's raytraces in a random spread that damages enemies </summary>
/// <param name="ShootOrigin">The starting point of the raytraces</param>
/// <param name="ForwardVector">The direction that the raytraces are aimed at</param>
void UShotgun::OnShoot(FVector ShootOrigin, FVector ForwardVector) {
	Super::OnShoot(ShootOrigin, ForwardVector);

	FVector Start = ShootOrigin;
	FVector End = (ForwardVector * BulletMaxTravelDistance) + Start;
	
	//Fire first pellet exactly where aiming
	FirePellet(Start, End);

	//Fire rest of the bullets in a cone spread
	for (int iBullet = 0; iBullet < ShellPelletAmount-1; iBullet++)
	{
		float VerticalSpread = FMath::FRandRange(-MaxVertSpreadAngle, MaxVertSpreadAngle);
		float HorizontalSpread = FMath::FRandRange(-MaxHoriSpreadAngle, MaxHoriSpreadAngle); if (bDisplayDebugMessages)
		{
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Purple, FString::Printf(TEXT("Vertical spread: %f"), VerticalSpread));
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Purple, FString::Printf(TEXT("Horizontal spread: %f"), HorizontalSpread));
		}
		FRotator BulletRotationMatrix = ForwardVector.ToOrientationRotator();
		FVector BulletForwardVector = BulletRotationMatrix.Add(VerticalSpread, HorizontalSpread, 0.0f).Vector();
		End = ((BulletForwardVector * BulletMaxTravelDistance) + Start);
		
		FirePellet(Start, End);
	}
}
