
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gun.h"
#include "MyProject/EnemyCharacter.h"
#include "Shotgun.generated.h"

/**
 *
 */
UCLASS()
class MYPROJECT_API UShotgun : public UGun
{
	GENERATED_BODY()

public:
	UShotgun();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


private:
	virtual void OnShoot(FVector ShootOrigin, FVector ForwardVector) override;
	void FirePellet(FVector StartPoint, FVector EndPoint);

	UPROPERTY(EditAnywhere)
		int ShellPelletAmount = 9;
	UPROPERTY(EditAnywhere)
		float BulletMaxTravelDistance = 10000;
	UPROPERTY(EditAnywhere)
		float MaxVertSpreadAngle = 5.0f;
	UPROPERTY(EditAnywhere)
		float MaxHoriSpreadAngle = 5.0f;
};
