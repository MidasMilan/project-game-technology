// Fill out your copyright notice in the Description page of Project Settings.

#include "Hook.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"

/// <summary>Sets the default values and creates the hook mesh used to visualise the end of the rope< / summary>
/// <param name="ObjectInitializer">The instance that can spawn new mesh objects into the world</param>
AHook::AHook(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	BaseMesh->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> BaseMeshAsset(TEXT("StaticMesh'/Game/StarterContent/Props/MaterialSphere.MaterialSphere'"));

	if (BaseMeshAsset.Object)
	{
		FVector MeshScale = FVector(0.3, 0.3, 0.3);
		BaseMesh->SetWorldScale3D(MeshScale);
		BaseMesh->SetStaticMesh(BaseMeshAsset.Object);
		BaseMesh->SetCollisionProfileName(TEXT("OverlapAll"));
	}
}

/// <summary>Calls when the game starts< / summary>
void AHook::BeginPlay()
{
	Super::BeginPlay();

}

/// <summary>Calles every frame< / summary>
void AHook::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

