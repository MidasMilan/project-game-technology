#pragma once
class IState
{

public:
	IState();
	~IState();

	virtual void Enter();
	virtual void Exit();
	virtual void Execute();
};
