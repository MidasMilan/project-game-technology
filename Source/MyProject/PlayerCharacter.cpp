// Fill out your copyright notice in the Description page of Project Settings.
#define print(text) if(GEngine) GEngine->AddOnScreenDebugMessage(-1,30, FColor::Cyan, text)
#define printf(text, fstring)  if(GEngine) GEngine->AddOnScreenDebugMessage(-1,30, FColor::Cyan, FString::Printf(TEXT(text), fstring))

#include "PlayerCharacter.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "Camera/CameraComponent.h"
#include "Engine/Engine.h"
#include "DrawDebugHelpers.h"
#include "PlayerCharacterMovementComponent.h"

// Sets default values
// More importantly uses the ObjectInitializer for setting our custom movement component.
APlayerCharacter::APlayerCharacter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer.SetDefaultSubobjectClass<UPlayerCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set the player's basic stats
	FullHealth = 100.0f;
	Health = FullHealth;

	// State
	PlayerMovementState = EPlayerMovementState::Walking;

	// Automaticly sets the player as the controller of the player pawn.
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	//Setup guns
	PistolPointer = CreateDefaultSubobject<UPistol>(TEXT("Pistol"));
	ShotgunPointer = CreateDefaultSubobject<UShotgun>(TEXT("Shotgun"));
	PlasmaGunPointer = CreateDefaultSubobject<UPlasmaGun>(TEXT("PlasmaGun"));
	BaseGunPointer = CreateDefaultSubobject<UBaseGun>(TEXT("BaseGun"));

	CurrentEquippedGun = EEquippedGun::Pistol;
	bShooting = false;
	bIsEquipping = false;

	// Set-up camera
	PlayerCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Player Camera"));
	PlayerCamera->SetupAttachment(GetCapsuleComponent());
	PlayerCamera->RelativeLocation = FVector(0, 0, BaseEyeHeight);
	PlayerCamera->bUsePawnControlRotation = true;

	// Grappling Hook
	GrapplingHook = CreateDefaultSubobject<UGrapplingHook>(TEXT("Grappling Hook"));
	GrapplingHook->SetPlayer(this);

	// State machine
	PlayerMovementState = EPlayerMovementState::Walking;
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	// declare NavTargetPoint
	FActorSpawnParameters TargetPointSpawnParams;
	TargetPoint = GetWorld()->SpawnActor<ATargetPoint>(ATargetPoint::StaticClass(), GetActorLocation(), GetActorRotation(), TargetPointSpawnParams);


	//AssignNavmeshTargetPoint("TargetPoint");

	//Assign weapon model pointers and configure models
	USceneComponent* cameraSceneComponent = (USceneComponent*)PlayerCamera;
	TArray<USceneComponent*> CamAttachedComponents = cameraSceneComponent->GetAttachChildren();

	for (int iCamPointer = 0; iCamPointer < CamAttachedComponents.Num(); iCamPointer++)
	{
		if (CamAttachedComponents[iCamPointer]->GetFName().IsEqual("SkeletalMesh"))
		{
			TArray<USceneComponent*> SkeletalAttachedComponents = CamAttachedComponents[iCamPointer]->GetAttachChildren();
			for (int jSkelPointer = 0; jSkelPointer < SkeletalAttachedComponents.Num(); jSkelPointer++)
			{
				if (SkeletalAttachedComponents[jSkelPointer]->GetFName().IsEqual("Pistols_A"))
				{
					APistol = SkeletalAttachedComponents[jSkelPointer];
				}
				if (SkeletalAttachedComponents[jSkelPointer]->GetFName().IsEqual("Shotgun_A"))
				{
					AShotgun = SkeletalAttachedComponents[jSkelPointer];
				}
				if (SkeletalAttachedComponents[jSkelPointer]->GetFName().IsEqual("White_AssaultRifle"))
				{
					APlasmagun = SkeletalAttachedComponents[jSkelPointer];
				}
			}
		}
	}

	EquipPistol();

	WorldTimerManager = &GetWorldTimerManager();
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	StateMachineUpdate();

	if (bShooting && (CurrentEquippedGun == EEquippedGun::Plasma))
	{
		PlasmaGunPointer->UPlasmaGun::TryShoot(GetActorLocation(), PlayerCamera->GetForwardVector());
	}

	if (GrapplingHook->GetCurrentState() == 2) {
		switch (PlayerMovementState)
		{
		case EPlayerMovementState::Walking:
			break;
		case EPlayerMovementState::Jumping:
			break;
		case EPlayerMovementState::Running:
			break;
		default:
			break;
		}

		HookSwing();
	}
	ClampPlayerSpeed();

	if (IsValid(TargetPoint))
	{
		MoveTargetPoint();
	}
	else
	{
		AssignNavmeshTargetPoint("NavTargetPoint");
	}
}

/// <summary>Assign's the point which detects the player on the navmesh </summary>
/// <param name = "ActorName">Name of the child actor</param>
void APlayerCharacter::AssignNavmeshTargetPoint(FName ActorName)
{
	USceneComponent* ThisActorSceneComp = (USceneComponent*)GetRootComponent();
	TArray<USceneComponent*> PlayerAttachedComponents = ThisActorSceneComp->GetAttachChildren();
	for (int iPointer = 0; iPointer < PlayerAttachedComponents.Num(); iPointer++)
	{
		if (PlayerAttachedComponents[iPointer]->GetFName().IsEqual(ActorName))
		{
			TargetPoint = (ATargetPoint*)PlayerAttachedComponents[iPointer];
		}
	}
}

/// <summary>State machine for player movement
/// </summary>
void APlayerCharacter::StateMachineUpdate()
{
	switch (CurrentState)
	{
	case EPlayerMovementState::Idle:
		//UE_LOG(LogTemp, Warning, TEXT("Idle"));
		break;
	case EPlayerMovementState::Walking:
		//UE_LOG(LogTemp, Warning, TEXT("Walking"));
		break;
	case EPlayerMovementState::Jumping:
		//UE_LOG(LogTemp, Warning, TEXT("Jumping"));
		break;
	case EPlayerMovementState::Running:
		//UE_LOG(LogTemp, Warning, TEXT("Running"));
		break;
	case EPlayerMovementState::Pushing:
		//UE_LOG(LogTemp, Warning, TEXT("Pushing"));
		PullTowards();
	default:
		break;
	}
}

void APlayerCharacter::SwitchState(EPlayerMovementState NewPlayerMovementState)
{
	CurrentState = NewPlayerMovementState;
}

APlayerCharacter::EPlayerMovementState APlayerCharacter::GetCurrentState()
{
	return CurrentState;
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Bind running.
	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &APlayerCharacter::Run);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &APlayerCharacter::StopRun);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &APlayerCharacter::Jump);

	// Mouse Shoot
	PlayerInputComponent->BindAction("GrapplingHookFire", IE_Pressed, this, &APlayerCharacter::ShootGrapplingHook);
	PlayerInputComponent->BindAction("GrapplingHookFire", IE_Released, this, &APlayerCharacter::ReleaseGrapplingHook);

	// Maneuvers when grappling hook is attached
	PlayerInputComponent->BindAction("PullTowards", IE_Pressed, this, &APlayerCharacter::PullTowards);
	PlayerInputComponent->BindAction("PullTowards", IE_Released, this, &APlayerCharacter::StopPulling);

	// Bind movement methods to the input events created in the project settings.
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("StepRight", this, &APlayerCharacter::StepRight);

	// Mouse look.
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	// Bind Gun methods
	PlayerInputComponent->BindAction("ShootGun", IE_Pressed, this, &APlayerCharacter::TryShootGun);
	PlayerInputComponent->BindAction("ShootGun", IE_Released, this, &APlayerCharacter::StopShootGun);
	PlayerInputComponent->BindAction("ReloadGun", IE_Pressed, this, &APlayerCharacter::TryReloadGun);
	PlayerInputComponent->BindAction("EquipPistol", IE_Pressed, this, &APlayerCharacter::EquipPistol);
	PlayerInputComponent->BindAction("EquipShotgun", IE_Pressed, this, &APlayerCharacter::EquipShotgun);
	PlayerInputComponent->BindAction("EquipPlasmaGun", IE_Pressed, this, &APlayerCharacter::EquipPlasmaGun);
	PlayerInputComponent->BindAction("EquipBaseGun", IE_Pressed, this, &APlayerCharacter::EquipBaseGun);
	PlayerInputComponent->BindAction("TestDamageUpgrade", IE_Pressed, this, &APlayerCharacter::GiveDamageUpgrade);
}

/// <summary>Try's to shoot a currently equipped semi-automatic gun</summary>
void APlayerCharacter::TryShootGun()
{
	bShooting = true;

	switch (CurrentEquippedGun) {
	default:
		GEngine->AddOnScreenDebugMessage(-1, 150.f, FColor::Red, FString::Printf(TEXT("Equipped gun not recognized!!!")));
		break;
	case EEquippedGun::Pistol:
		PistolPointer->UPistol::TryShoot(GetActorLocation(), PlayerCamera->GetForwardVector());
		break;
	case EEquippedGun::Shotgun:
		ShotgunPointer->UShotgun::TryShoot(GetActorLocation(), PlayerCamera->GetForwardVector());
		break;
	case EEquippedGun::Plasma:
		break;
	case EEquippedGun::Base:
		BaseGunPointer->UBaseGun::TryFire(GetActorLocation(), PlayerCamera->GetForwardVector());
		break;
	}
}

/// <summary>Stops to try shooting the currently equipped gun</summary>
void APlayerCharacter::StopShootGun()
{
	bShooting = false;
}

/// <summary>Equips the pistol</summary>
void APlayerCharacter::EquipPistol()
{
	EquipGun(EEquippedGun::Pistol);
}

/// <summary>Equips the Shotgun</summary>
void APlayerCharacter::EquipShotgun()
{
	EquipGun(EEquippedGun::Shotgun);
}

/// <summary>Equips the Plasma gun</summary>
void APlayerCharacter::EquipPlasmaGun()
{
	EquipGun(EEquippedGun::Plasma);
}

/// <summary>Equips the Plasma gun</summary>
void APlayerCharacter::EquipBaseGun()
{
	EquipGun(EEquippedGun::Base);
}

/// <summary>Gives a damageupgrade to a gun</summary>
void APlayerCharacter::GiveDamageUpgrade()
{
	UDamageUpgrade* DamageUpgradePointer = NewObject<UDamageUpgrade>(this, "DamageUpgrade");
	DamageUpgradePointer->RegisterComponentWithWorld(GetWorld());
	GEngine->AddOnScreenDebugMessage(-1, 150.f, FColor::Red, FString::Printf(TEXT("Thing made!!!")));
}

/// <summary>Stops the equipping animation</summary>
void APlayerCharacter::StopEquip()
{
	bIsEquipping = false;
}

/// <summary>Try's to reload the currently equipped gun</summary>
void APlayerCharacter::TryReloadGun()
{
	switch (CurrentEquippedGun) {
	default:
		GEngine->AddOnScreenDebugMessage(-1, 150.f, FColor::Red, FString::Printf(TEXT("Equipped gun not recognized!!!")));
		break;
	case EEquippedGun::Pistol:
		PistolPointer->TryReload();
		break;
	case EEquippedGun::Shotgun:
		ShotgunPointer->TryReload();
		break;
	case EEquippedGun::Plasma:
		PlasmaGunPointer->TryReload();
		break;
	case EEquippedGun::Base:
		BaseGunPointer->TryReloading();
		break;
	}
}

/// <summary>Equips a gun for shooting logic and displays the right model </summary>
/// <param name = "EEquippedGun">The gun that gets equipped</param>
void APlayerCharacter::EquipGun(EEquippedGun Gun)
{
	CurrentEquippedGun = Gun;

	if (!GetWorldTimerManager().IsTimerActive(EquipWeaponTimer))
	{
		bIsEquipping = true;
		GetWorldTimerManager().SetTimer(EquipWeaponTimer, this, &APlayerCharacter::StopEquip, WeaponEquipAnimLength, false);
	}
	FVector EquippedGunScale = FVector(1.0f, 1.0f, 1.0f);
	FVector UnequippedGunScale = FVector(0.01f, 0.01f, 0.01f);
	switch (Gun) {
	default:
		GEngine->AddOnScreenDebugMessage(-1, 150.f, FColor::Red, FString::Printf(TEXT("Gun to be equipped not recognized!!!")));
		break;
	case EEquippedGun::Pistol:
		APistol->SetWorldScale3D(EquippedGunScale);
		AShotgun->SetWorldScale3D(UnequippedGunScale);
		APlasmagun->SetWorldScale3D(UnequippedGunScale);
		break;
	case EEquippedGun::Shotgun:
		APistol->SetWorldScale3D(UnequippedGunScale);
		AShotgun->SetWorldScale3D(EquippedGunScale);
		APlasmagun->SetWorldScale3D(UnequippedGunScale);
		break;
	case EEquippedGun::Plasma:
		APistol->SetWorldScale3D(UnequippedGunScale);
		AShotgun->SetWorldScale3D(UnequippedGunScale);
		APlasmagun->SetWorldScale3D(EquippedGunScale);
		break;
	case EEquippedGun::Base:
		APistol->SetWorldScale3D(EquippedGunScale);
		AShotgun->SetWorldScale3D(UnequippedGunScale);
		APlasmagun->SetWorldScale3D(UnequippedGunScale);
		break;
	}
}

UPlayerCharacterMovementComponent* APlayerCharacter::GetPlayerCharacterMovement() const
{
	return Cast<UPlayerCharacterMovementComponent>(GetCharacterMovement());
}

// The methods below are triggered by the PlayerInputComponent.
// Its their job now to get the CharacterMovementComponent and add vectors for movement.

void APlayerCharacter::MoveForward(float AxisValue)
{
	float scale = AxisValue * MovementSpeed;
	FVector directionVector = GetActorForwardVector();
	AddMovementInput(directionVector, scale);
}

void APlayerCharacter::StepRight(float AxisValue)
{
	AddMovementInput(GetActorRightVector(), AxisValue * MovementSpeed);
}

void APlayerCharacter::Run()
{
	if (GetCharacterMovement()->IsMovingOnGround())
	{
		GetCharacterMovement()->MaxWalkSpeed *= RunMultiplier;

		if (GetCurrentState() != EPlayerMovementState::Pushing)
			SwitchState(EPlayerMovementState::Running);
	}
}

void APlayerCharacter::StopRun()
{
	if (GetCharacterMovement()->IsMovingOnGround())
	{
		GetCharacterMovement()->MaxWalkSpeed /= RunMultiplier;

		if (GetCurrentState() != EPlayerMovementState::Pushing)
			SwitchState(EPlayerMovementState::Idle);
	}
}

void APlayerCharacter::Jump()
{
	if (GetCharacterMovement()->IsMovingOnGround())
	{
		LaunchCharacter(FVector::UpVector * GetCharacterMovement()->JumpZVelocity, false, true);

		if (GetCurrentState() != EPlayerMovementState::Pushing)
			SwitchState(EPlayerMovementState::Jumping);
	}
}

void APlayerCharacter::MoveTargetPoint()
{
	FHitResult OutHit;
	// TODO: Start position to be replaced with grappling hook starting position
	FVector Start = GetActorLocation();

	FVector DirectionVector = FVector(0, 0, -1);
	//TODO: Switch magic number with variable
	FVector End = ((DirectionVector * 50000) + Start);
	FCollisionQueryParams CollisionParams;

	//DrawDebugLine(GetWorld(), Start, End, FColor::Green, true);

	bool isHit = GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams);

	TargetPoint->SetActorLocation(OutHit.ImpactPoint);
}

ATargetPoint* APlayerCharacter::GetTargetPoint()
{
	return TargetPoint;
}

void APlayerCharacter::ClampPlayerSpeed() {
	if (GetCharacterMovement()->Velocity.Size() > MaxPlayerSpeed) {
		GetCharacterMovement()->Velocity = GetCharacterMovement()->Velocity.GetClampedToMaxSize(MaxPlayerSpeed);
	}
}

/// <summary>Get's the BaseGun pointer</summary>
UBaseGun* APlayerCharacter::GetBaseGunPointer()
{
	return BaseGunPointer;
}

/// TODO comment
float APlayerCharacter::GetHealth()
{
	return Health;
}

/// TODO comment
FText APlayerCharacter::GetHealthAsText()
{
	int32 HealthInt = FMath::RoundHalfFromZero(Health);
	FString HealthString = FString::FromInt(HealthInt);
	return FText::FromString(HealthString);
}


/// TODO comment
void APlayerCharacter::UpdateHealth(float HealthChange)
{
	Health += HealthChange;
	Health = FMath::Clamp(Health, 0.0f, FullHealth);
}

void APlayerCharacter::ShootGrapplingHook()
{
	if (GrapplingHook->GetCurrentState() == 0) {
		GrapplingHook->Shoot(GetPlayerCamera()->GetForwardVector(), GetActorLocation(), GetWorld(), this);
	}
}

void APlayerCharacter::ReleaseGrapplingHook()
{
	if (GrapplingHook->GetCurrentState() == 2 || GrapplingHook->GetCurrentState() == 1)
	{
		GrapplingHook->Release();
		FVector speedBoost = (GetCharacterMovement()->Velocity * EndSwingSpeedBoost) - GetCharacterMovement()->Velocity;
		if (speedBoost.Size() > MaxSpeedBoost) {
			//TODO: Normalize speedboost and speedboost * maxspeedboost
			speedBoost = speedBoost.GetSafeNormal() * MaxSpeedBoost;
		}
		GetCharacterMovement()->Velocity = GetCharacterMovement()->Velocity + speedBoost;
	}
}

/// <summary>
/// When the grappling hook is attached, pulls the player towards the anchor point
/// The further away the player is the strong the pull is.
/// <summary> 
void APlayerCharacter::HookSwing()
{
	// The vector which finds the direction towards the anchor point from the player position
	FVector directionVector = GetActorLocation() - GrapplingHook->GetRopeAnchorPoint();

	// Pull the player towards the hook
	float distanceBeyondRopeLimit = directionVector.Size() - GrapplingHook->GetCurrentRopeLength();
	if (distanceBeyondRopeLimit > 0) {
		SetActorLocation(GetActorLocation() - (directionVector.GetSafeNormal() * distanceBeyondRopeLimit));

		FVector tempVector = FVector::CrossProduct(directionVector, GetCharacterMovement()->Velocity);
		GetCharacterMovement()->Velocity = -FVector::CrossProduct(directionVector, tempVector).GetSafeNormal() * GetCharacterMovement()->Velocity.Size();
	}

	if (distanceBeyondRopeLimit < 0) {
		GrapplingHook->SetCurrentRopeLength(directionVector.Size());
	}
}

/// <summary>Pushes player towards the target
/// <para>This method fires when PushTowards key event starts.</para>
/// </summary>
void APlayerCharacter::PullTowards()
{
	if (GrapplingHook->GetCurrentState() == 2)
	{
		GrapplingHook->DecreaseCurrentRopeLength((PullingSpeed));
		SwitchState(EPlayerMovementState::Pushing);
	}
}

/// <summary>Stops pushing player towards the target
/// <para>This method fires when PushTowards key release event starts.</para>
/// </summary>
void APlayerCharacter::StopPulling()
{
	/*if (GrapplingHook->GetCurrentState() == 2 && GetCurrentState() == EPlayerMovementState::Pushing)
	{
		GetCharacterMovement()->Velocity = FVector::ZeroVector;
		SwitchState(EPlayerMovementState::Idle);
	}*/
	SwitchState(EPlayerMovementState::Idle);
}

APlayerCharacter::~APlayerCharacter()
{
	//delete &GunPointer;
	Super::Destroy();
}