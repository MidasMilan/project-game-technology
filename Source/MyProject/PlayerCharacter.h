// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "StateMachine.h"
#include "PlayerCharacterMovementComponent.h"
#include "MyProject/GrapplingHook.h"
#include "MyProject/Gun.h"
#include "MyProject/Gun/BaseGun.h"
#include "MyProject/Gun/Pistol.h"
#include "MyProject/Gun/Shotgun.h"
#include "MyProject/Gun/PlasmaGun.h"
#include "MyProject/Gun/DamageUpgrade.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectGlobals.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "PlayerCharacter.generated.h"

class UCameraComponent;
class APlayerCharacter;

UENUM(BlueprintType)
enum class EEquippedGun : uint8
{
	Pistol	UMETA(DisplayName = "Pistol"),
	Shotgun	UMETA(DisplayName = "Shotgun"),
	Plasma	UMETA(DisplayName = "Plasma"),
	Base	UMETA(DisplayName = "Base")
};

UCLASS()
class MYPROJECT_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter(const FObjectInitializer& ObjectInitializer);
	APlayerCharacter();
	~APlayerCharacter();

	enum EPlayerMovementState :uint8
	{
		Idle,
		Walking,
		Running,
		Jumping,
		Pushing,
	};

	EPlayerMovementState PlayerMovementState;
	void SwitchState(EPlayerMovementState NewPlayerMovementState);
	EPlayerMovementState GetCurrentState();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	FTimerHandle InputTimeHandle;
	EPlayerMovementState CurrentState;
	void StateMachineUpdate();
	FHitResult *OutHit = nullptr;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		TEnumAsByte<EEquippedGun> CurrentEquippedGun;
	void EquipGun(EEquippedGun newState);

private:
	void HookSwing();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPlayerCharacterMovementComponent * GetPlayerCharacterMovement() const;

	// Movement functions
	void MoveForward(float AxisValue);
	void StepRight(float AxisValue);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapons")
		UPistol* PistolPointer;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapons")
		UShotgun* ShotgunPointer;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapons")
		UPlasmaGun* PlasmaGunPointer;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapons")
		UBaseGun* BaseGunPointer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		bool bShooting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		bool bIsEquipping;

	FTimerHandle EquipWeaponTimer;

	UPROPERTY(EditAnywhere)
		USceneComponent* APistol;
	UPROPERTY(EditAnywhere)
		USceneComponent* AShotgun;
	UPROPERTY(EditAnywhere)
		USceneComponent* APlasmagun;
	FTimerManager* WorldTimerManager;

	void TryShootGun();
	void StopShootGun();
	void TryReloadGun();
	void EquipPistol();
	void EquipShotgun();
	void EquipPlasmaGun();
	void EquipBaseGun();
	void GiveDamageUpgrade();
	void StopEquip();
	void Run();
	void StopRun();
	void Jump();
	void MoveTargetPoint();
	void AssignNavmeshTargetPoint(FName ActorName);
	ATargetPoint* GetTargetPoint();

	void ClampPlayerSpeed();

	UFUNCTION(BlueprintPure, Category = "Weapon")
		UBaseGun* GetBaseGunPointer();

	UFUNCTION(BlueprintPure, Category = "Weapon")
		float GetHealth();

	UFUNCTION(BlueprintCallable, Category = "Health")
		void UpdateHealth(float HealthChange);

	UFUNCTION(BlueprintPure, Category = "Health")
		FText GetHealthAsText();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float FullHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float Health;

	UPROPERTY(EditAnywhere)
		ATargetPoint* TargetPoint;

	UPROPERTY(EditAnywhere)
		float EndSwingSpeedBoost = 1;

	UPROPERTY(EditAnywhere)
		float MaxSpeedBoost = 300;

	UPROPERTY(EditAnywhere)
		float MaxPlayerSpeed = 5000;

	UPROPERTY(VisibleAnywhere)
		UCameraComponent* PlayerCamera;

	UPROPERTY(VisibleAnywhere)
		UGrapplingHook* GrapplingHook;

	UPROPERTY(EditAnywhere)
		float MovementSpeed;

	UPROPERTY(EditAnywhere)
		float RunMultiplier;

	UPROPERTY(EditAnywhere)
		float WeaponEquipAnimLength = 0.1f;

	UCameraComponent* GetPlayerCamera() const { return PlayerCamera; };

	// This works ontop of the regular rope distance
	UPROPERTY(EditAnywhere)
		float RopeBreakDistance = 500;
	UPROPERTY(EditAnywhere, meta = (UIMin = "0.005", UIMax = "0.1"))
		float RopeSwingPower = 0.025;

	UPROPERTY(EditAnywhere, meta = (UIMin = "0", UIMax = "100"))
		float PullingSpeed = 25;
	UPROPERTY(VisibleAnywhere)
		UPlayerCharacterMovementComponent* PlayerCharacterMovementComponent;


	/*enum EPlayerMovementState :uint8
	{
		Walking = 0,
		Running,
		Jumping,
	};*/

	//EPlayerMovementState PlayerMovementState;

	void ShootGrapplingHook();
	void ReleaseGrapplingHook();
	void PullTowards();
	void StopPulling();
};