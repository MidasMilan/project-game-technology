// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerCharacterMovementComponent.h"

void UPlayerCharacterMovementComponent::InitializeComponent()
{
    Super::InitializeComponent();
}

/// <summary>
/// Legacy style movement based on Quake and Source.
/// </summary>
void UPlayerCharacterMovementComponent::Accelerate(FVector DesiredDirection, float AxisValue)
{
    float AddSpeed;         // 
    float AccelSpeed;       // 
    float CurrentSpeed;     // 

    if (AxisValue != 0)
    {
        DesiredDirection.Normalize();
        // Will be 0 when player has no velocity, and uses the maximum walking speed.
        CurrentSpeed = FVector::DotProduct(Velocity, DesiredDirection);
        AddSpeed = MaxWalkSpeed - CurrentSpeed;

        AccelSpeed = FMath::Max(FMath::Min(AddSpeed, (float)(MaxAcceleration * FApp::GetFixedDeltaTime())), 0.0f);

        Velocity += (DesiredDirection * AccelSpeed) * AxisValue;

        if (GEngine)
            GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::White, FString::Printf(TEXT("Velocity: %f"), Velocity.Size()));

        if (GEngine)
            GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::White, FString::Printf(TEXT("AxisValue: %f"), AxisValue));
    }
}