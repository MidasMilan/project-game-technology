// Fill out your copyright notice in the Description page of Project Settings.

#include "Doorway.h"

/// <summary>
/// ADoorway is an Actor that gets placed within the Blueprint Editor, and indexed by the AGenerated room class. The doorway is essential for connecting two AGeneratedrooms within the LevelLayoutGenerator Generate script. 
/// </summary>
ADoorway::ADoorway()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADoorway::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ADoorway::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

/// <summary>
/// Returns the directionVector of this Doorway actor
/// </summary>
FVector ADoorway::getDirectionVector()
{
	return GetActorForwardVector();
}


