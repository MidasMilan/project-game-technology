// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemySpawnPoint.h"
#include "MyProject/EnemyCharacter.h"
#include "MyProject/TagController.h"

// Sets default values
AEnemySpawnPoint::AEnemySpawnPoint()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bIsSpawned = false;
}

// Called when the game starts or when spawned
void AEnemySpawnPoint::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AEnemySpawnPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsSpawned == false)
	{
		ACharacter* myCharacter = UGameplayStatics::GetPlayerCharacter((UObject*)GetWorld(), 0);
		if (IsValid(myCharacter))
		{
			//Instantiate enemy
			FVector SpawnPosition = GetActorLocation() + (GetActorUpVector() * VerticalSpawnOffset);
			FRotator SpawnOrientation = GetActorForwardVector().ToOrientationRotator();
			FTransform EnemyTransform = FTransform::FTransform(SpawnOrientation, SpawnPosition);

			AEnemyCharacter* EnemyPointer = GetWorld()->SpawnActorDeferred<AEnemyCharacter>(Enemy, EnemyTransform);
			EnemyPointer->SetAIController(TagController);
			EnemyPointer->FinishSpawning(EnemyTransform);

			bIsSpawned = true;
		}
	}
}

