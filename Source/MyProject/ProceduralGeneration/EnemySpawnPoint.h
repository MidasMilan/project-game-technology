// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemySpawnPoint.generated.h"

class AEnemyCharacter;
class ATagController;

UCLASS()
class MYPROJECT_API AEnemySpawnPoint : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AEnemySpawnPoint();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere)
		float VerticalSpawnOffset = 50.0f;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AEnemyCharacter> Enemy;
	UPROPERTY(EditAnywhere)
		TSubclassOf<ATagController> TagController;
	bool bIsSpawned = false;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
