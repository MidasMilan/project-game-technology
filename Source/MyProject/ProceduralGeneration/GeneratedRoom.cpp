// Fill out your copyright notice in the Description page of Project Settings.

#include "GeneratedRoom.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Public/EngineUtils.h"
#include "Engine/Engine.h"



/// <summary>
/// AGeneratedRoom is an actor class that gets placed by ALevelLayoutGenerator. It indexes the doors that the blueprint holds
/// </summary>
AGeneratedRoom::AGeneratedRoom()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AGeneratedRoom::BeginPlay()
{
	Super::BeginPlay();
	Doorways = FindDoors();
}

/// <summary>
/// Finds all instantiated door actors within the room blueprint.
/// </summary>
TArray<ADoorway*> AGeneratedRoom::FindDoors()
{
	TArray<ADoorway*> doorways;
	UWorld* const World = GetWorld();
	for (TActorIterator<ADoorway> It(World); It; ++It)
	{
		ADoorway* currentDoor = *It;
		if (currentDoor && currentDoor->GetAttachParentActor() == this)
		{
			doorways.Add(currentDoor);
		}
	}

	return doorways;
}

/// <summary>
/// Returns the list size of the Doorways List
/// </summary>
int AGeneratedRoom::getDoorwaysSize()
{
	return Doorways.Num();
}

/// <summary>
/// Returns a door with given index
/// </summary>
ADoorway* AGeneratedRoom::getDoor(int number)
{
	ADoorway* door = Doorways[number];
	return door;
}

/// <summary>
/// Deletes a door with given index
/// </summary>
void AGeneratedRoom::DeleteDoor(int index)
{
	Doorways.RemoveAt(index);
}

/// <summary>
/// Adds a door at given index
/// </summary>
void AGeneratedRoom::AddDoor(ADoorway* door, int index)
{
	Doorways.Insert(door, index);
}


// Called every frame
void AGeneratedRoom::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
