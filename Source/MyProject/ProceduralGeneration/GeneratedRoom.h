// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyProject/ProceduralGeneration/Doorway.h"
#include "MyProject/ProceduralGeneration/Floor.h"
#include "GeneratedRoom.generated.h"

UCLASS()
class MYPROJECT_API AGeneratedRoom : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGeneratedRoom();
	int getDoorwaysSize();
	ADoorway* getDoor(int index);
	void AddDoor(ADoorway* door, int index);
	void DeleteDoor(int index);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<ADoorway*> Doorways;

	TArray<ADoorway*> FindDoors();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
