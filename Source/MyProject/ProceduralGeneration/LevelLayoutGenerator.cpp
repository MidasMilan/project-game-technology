// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelLayoutGenerator.h"
#include "Engine/Engine.h"

// Sets default values
ALevelLayoutGenerator::ALevelLayoutGenerator()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ALevelLayoutGenerator::BeginPlay()
{
	Super::BeginPlay();

	Generate();
}


/// <summary> This code takes a room, places it and then finds a new room to put down next to it. It tries placing the room a few times, and if it fails it goes back one step in the process.
/// </summary>
void ALevelLayoutGenerator::Generate()
{
	//Index of the room that is being put down.
	int currentRoom = 0;

	//If you did not enter a seed, it will generate it's own seed.
	if (seed == 0)
	{
		seed = FMath::RandRange(0, 9000000);
	}

	//Sets random seed. Used so we can regenerate the same rooms (Both for testing and fun).
	random = FRandomStream::FRandomStream(seed);

	//Set amount of rooms to generate
	roomAmount = random.FRandRange(minRoomAmount, maxRoomAmount);


	//Step 1: Place the starting room at the start position.
	FVector realPosition = FVector(0, 0, 0);
	roomActors.Add(GetWorld()->SpawnActor<AGeneratedRoom>(startRoom, realPosition, FRotator::ZeroRotator));

	TArray<ADoorway*>Doors;
	Doors.SetNum(roomAmount * 2);

	for (int i = 0; i < roomAmount; i++)
	{

		//If the roomactor is defined.
		if (roomActors[i])
		{
			TArray<AActor *> overlapActors;
			int attempts = 0;
			int maxAttempts = 5;

			bool overlapWithRoom = false;

			do
			{
				int doorIndex = i * 2;
				int doorIndexTwo = doorIndex + 1;

				//Reset the overlap actors
				overlapActors.Empty();
				int firstDoorIndex = random.FRandRange(0, roomActors[i]->getDoorwaysSize());
				int secondDoorIndex = 0;
				Doors[doorIndex] = roomActors[i]->getDoor(firstDoorIndex);

				//Find second room.
				int r = random.FRandRange(0, AllRooms.Num());

				TSubclassOf <AGeneratedRoom> roomToPlace;

				if (i == roomAmount - 1)
				{
					roomToPlace = goalRoom;
				}
				else
				{
					roomToPlace = AllRooms[r];
				}
				//Place it
				roomActors.Add(GetWorld()->SpawnActor<AGeneratedRoom>(roomToPlace, realPosition, FRotator::ZeroRotator));

				//Choose a random door and assign it to second door.
				secondDoorIndex = random.FRandRange(0, roomActors[i + 1]->getDoorwaysSize());
				Doors[doorIndexTwo] = roomActors[i + 1]->getDoor(secondDoorIndex);

				//Rotate the rooms
				RotateRoom(i, Doors[doorIndex], Doors[doorIndexTwo]);

				//position the doors on each other so the rooms connect;
				PositionRoom(i, Doors[doorIndex], Doors[doorIndexTwo]);

				roomActors[i + 1]->GetOverlappingActors(overlapActors);

				overlapWithRoom = false;
				//If there is overlap with an existing room.
				if (overlapActors.Num() > 0)
				{
					//Check for tag
					for (int j = 0; j < overlapActors.Num(); j++)
					{
						if (overlapActors[j]->ActorHasTag(TEXT("Room")))
						{
							overlapWithRoom = true;
						}
					}

					if (overlapWithRoom)
					{
						roomActors[i + 1]->Destroy();
						roomActors.RemoveAt(i + 1);
						attempts++;
						//If the first room only has two possible exit or none, then place another room instead of that one.
						if (attempts > maxAttempts)
						{
							roomActors[i]->Destroy();
							roomActors.RemoveAt(i);
							i--;
							attempts = 0;
						}
					}
				}
			} while (overlapActors.Num() > 0 && overlapWithRoom);
		}
	}

	//Go over every single door that should connect, and replace them for doors that the player can walk through
	for (int i = 0; i < roomAmount; i++)
	{
		int doorIndex = i * 2;
		int doorIndexTwo = doorIndex + 1;


		//Remove all "possible doors" and replace them for the new door blueprints.
		GetWorld()->SpawnActor<ADoorway>(replacementDoor, Doors[doorIndex]->GetActorLocation(), Doors[doorIndex]->GetActorRotation());
		GetWorld()->SpawnActor<ADoorway>(replacementDoor, Doors[doorIndexTwo]->GetActorLocation(), Doors[doorIndexTwo]->GetActorRotation());

		Doors[doorIndex]->Destroy();
		Doors[doorIndexTwo]->Destroy();
	}
}


/// <summary> Rotates the second room to face the first room, taking the doors as their forward facing directions.
/// </summary>
void ALevelLayoutGenerator::RotateRoom(int roomIndex, ADoorway* firstDoor, ADoorway* secondDoor)
{
	//Rotate and position the entire room to have the door face the other door
	FVector FirstDoorForward = firstDoor->getDirectionVector();
	FVector SecondDoorForward = secondDoor->getDirectionVector();

	//Tolerance is there because floats are difficult to compare to each other, as often a decimal number is not exactly 0. And the vectors we use are filled with floats.
	float tolerance = 0.1;

	//Get the angle between the two forward facing vectors.
	int angle = math.RoundToInt(math.RadiansToDegrees(math.Acos(FVector::DotProduct(FirstDoorForward, SecondDoorForward *-1))));

	//Set a possible extra rotation. I will use this if the second door is facing the exact opposite way of the first door.
	int extraRotation = 180;

	FRotator rotation = FRotator::ZeroRotator;

	//If the doors are not already facing eachother.
	if (!FirstDoorForward.Equals(SecondDoorForward *-1, tolerance))
	{
		//If the doors are not facing the same direction
		if (!SecondDoorForward.Equals(FirstDoorForward, tolerance))
		{
			//Add the angle we gotten between te vectors to the rotation.
			rotation.Add(0.0f, angle, 0.0f);
			roomActors[roomIndex + 1]->SetActorRotation(rotation);
			SecondDoorForward = secondDoor->GetActorForwardVector();
			//Add an extra 90 degrees to extra rotation. 
			//so if the room starts facing the same direction as the first one after this step, it will be corrected to face the opposite direction.
			extraRotation += 90;
			rotation = FRotator::ZeroRotator;
		}
		//If the doors are facing the same direction.
		if (SecondDoorForward.Equals(FirstDoorForward, tolerance))
		{
			//Add 180 degrees (extraRotation) so the room faces the right direction again.
			rotation.Add(0.0f, extraRotation, 0.0f);
			roomActors[roomIndex + 1]->SetActorRotation(rotation);
		}
	}
}

/// <summary> Places the first room on the position of the second room. In such a way that the two doors connect.
/// </summary>
void ALevelLayoutGenerator::PositionRoom(int roomIndex, ADoorway* firstDoor, ADoorway* secondDoor)
{
	FVector locationOne = firstDoor->GetActorLocation();
	FVector locationTwo = secondDoor->GetActorLocation();

	roomActors[roomIndex + 1]->AddActorWorldOffset(locationOne - locationTwo);
}
