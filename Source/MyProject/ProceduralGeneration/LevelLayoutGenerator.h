// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyProject/ProceduralGeneration/GeneratedRoom.h"
#include "LevelLayoutGenerator.generated.h"




UCLASS()
class MYPROJECT_API ALevelLayoutGenerator : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ALevelLayoutGenerator();

private:

	//Methods
	void Generate();
	/// <summary> Rotates the a room to face  another room, taking the doors as their forward facing directions.
	/// </summary>
	void RotateRoom(int roomIndex, ADoorway* firstDoor, ADoorway* secondDoor);
	void PositionRoom(int roomIndex, ADoorway* firstDoor, ADoorway* secondDoor);

	//Properties to be set within the editor.
	UPROPERTY(EditAnywhere)
		int seed;

	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<AGeneratedRoom>> AllRooms;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AGeneratedRoom> startRoom;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AGeneratedRoom> goalRoom;
	UPROPERTY(EditAnywhere)
		TSubclassOf<ADoorway> replacementDoor;

	UPROPERTY(EditAnywhere)
		int maxRoomAmount;
	UPROPERTY(EditAnywhere)
		int minRoomAmount;

	int roomAmount;

	FMath math;

	FVector roomRealSize;

	TArray<AGeneratedRoom*> roomActors;

	FRandomStream random;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

};
