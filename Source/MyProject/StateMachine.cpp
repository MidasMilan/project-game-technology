#include "StateMachine.h"

StateMachine::StateMachine()
{
}

StateMachine::StateMachine(IState StartingState)
{
	ActiveState = StartingState;
}

StateMachine::~StateMachine()
{
}

IState StateMachine::GetActiveState()
{
	return ActiveState;
}

void StateMachine::SetActiveState(IState NewState)
{
	ActiveState = NewState;
}

void StateMachine::ChangeState(IState NewState)
{
	ActiveState.Exit();
	ActiveState = NewState;
	ActiveState.Enter();
}

void StateMachine::Tick()
{
	ActiveState.Execute();
}
