#pragma once

#include "IState.h"

class StateMachine
{

public:
	StateMachine();
	StateMachine(IState StartingState);
	~StateMachine();

	IState GetActiveState();
	void SetActiveState(IState NewState);
	void ChangeState(IState NewState);
	void Tick();

private:
	IState ActiveState;
};
