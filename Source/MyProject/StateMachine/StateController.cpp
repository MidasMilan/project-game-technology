// Fill out your copyright notice in the Description page of Project Settings.

#include "StateController.h"

StateController::StateController()
= default;

StateController::~StateController()
= default;

/// <summary> 
/// Triggers the active state's update code every tick 
/// </summary>
void StateController::UpdateState()
{
	// Broadcast the on Exit events of the old active states
	for (auto i = 0; i < _activeState.Num(); i++)
	{
		_activeState[i].OnStateUpdate().Broadcast();
	}
}

/// <summary> 
/// Returns the current active state(s) as an array
/// </summary>
TArray<StateMachineState>* StateController::GetActiveState()
{
	return &_activeState;
}

/// <summary> 
/// Returns the current active state(s) as an array
/// </summary>
TArray<StateMachineState>* StateController::GetPreviousState()
{
	return &_previousState;
}

/// <summary> Switches the state of the state machine to a single or multiple states based on the given parameter. 
/// This overrides the current one(s) instantly and calls their respective OnStateEnter and OnStateExit events.
/// The previous state is also set.
/// </summary>
/// <param name="newState"> The state(s) to switch to </param>
void StateController::SwitchState(const TArray<StateMachineState>& newState)
{
	// Broadcast the on Exit events of the old active states
	for (auto i = 0; i < _activeState.Num(); i++)
	{
		_activeState[i].OnStateExit().Broadcast();
	}

	// Set the new previous state
	_previousState = newState;

	// Broadcast the OnEnter events for the new active states
	for (auto i = 0; i < _activeState.Num(); i++)
	{
		_activeState[i].OnStateEnter().Broadcast();
	}
}

/// <summary> 
/// Checks whether a single state is active
/// </summary>
/// <param name="checkState"> The state to check against </param>
bool StateController::CheckForStateActive(StateMachineState checkState)
{
	auto returnCheck = false;

	// Loop through all of the active states and compare them with the parameter state
	for (auto i = 0; i < _activeState.Num(); i++)
	{
		if (checkState.GetName().Compare(_activeState[i].GetName()))
		{
			returnCheck = true;
		}
	}
	return returnCheck;
}

/// <summary> 
/// Checks whether multiple states are all active
/// </summary>
/// <param name="checkedStates"> Multiple states to check whether they are all active </param>
bool StateController::CheckForStatesActive(const TArray<StateMachineState>& checkedStates)
{
	// Loop through all of the active states and compare them with the parameter state
	for (auto i = 0; i < checkedStates.Num(); i++)
	{
		if (!CheckForStateActive(checkedStates[i]))
		{
			return false;
		}
	}

	// When it passed all of the active states as active, return true
	return true;
}
