// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StateMachineState.h"

/// <summary>
/// This class regulates the states and their event calls. It also switches the states and keeps track of the
/// current active and previous states. The state update gets called in the OnTick function activating the code in the active states.
/// This class exists to keep the controlling of the states separated from the main component, which makes is easier to read and hides the implementation better,
/// without losing functionality.
/// </summary>
class MYPROJECT_API StateController
{
protected:
	TArray<StateMachineState> _activeState;
	TArray<StateMachineState> _previousState;

public:
	StateController();
	~StateController();

	TArray<StateMachineState>* GetActiveState();
	TArray<StateMachineState>* GetPreviousState();

	bool CheckForStateActive(StateMachineState checkState);
	bool CheckForStatesActive(const TArray<StateMachineState>& checkedStates);

	void SwitchState(const TArray<StateMachineState>& newState);
	void UpdateState();
};
