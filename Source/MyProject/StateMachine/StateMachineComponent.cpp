// Fill out your copyright notice in the Description page of Project Settings.

#include "StateMachineComponent.h"

using namespace std;

/// <summary> Sets default values for this component's properties </summary>
UStateMachineComponent::UStateMachineComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


/// <summary> Called when the game starts </summary>
void UStateMachineComponent::BeginPlay()
{
	Super::BeginPlay();
}


/// <summary> Called every frame </summary>
void UStateMachineComponent::TickComponent(float deltaTime, ELevelTick tickType, FActorComponentTickFunction* thisTickFunction)
{
	Super::TickComponent(deltaTime, tickType, thisTickFunction);
	_stateController.UpdateState();
}

/// <summary> Switches the state of the state machine to a single or multiple states based on the given parameter. 
/// This overrides the current one(s) instantly and calls their respective OnStateEnter and OnStateExit events.
/// 
/// This method calls the OnStateSwitch event at the start as well, passing on the state change to the state controller.</summary>
/// <param name="newState"> The state(s) to switch to </param>
TArray<StateMachineState> UStateMachineComponent::SwitchState(const TArray<StateMachineState>& newState)
{
	// Activate all delegates within the switch event
	OnStateSwitch().Broadcast();

	// Let the controller handle the switching of the states
	_stateController.SwitchState(newState);

	// Return the current state for ease of use
	return newState;
}

