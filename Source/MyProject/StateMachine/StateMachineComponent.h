// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "StateMachineState.h"
#include "StateController.h"
#include "StateMachineComponent.generated.h"

/// <summary>
/// The StateMachineComponent is a component that inherits from an UActorComponent from Unreal, allowing it to be put on Actors in the editor for ease of use.

/// The component has an event which gets fired when states are being switched, allowing the developer to hook into this behaviour for special occasions.

/// The component also has a state controller, which keeps track of the current state, previous state and the switching of states.
/// This keeps the active behaviour of the state machine away from the component.
/// </summary>
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class MYPROJECT_API UStateMachineComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UStateMachineComponent();

	DECLARE_EVENT(FLayerViewModel, FChangedEvent)

	FChangedEvent& OnStateSwitch() { return _stateSwitchEvent; };

protected:
	void BeginPlay() override;

	FChangedEvent _stateSwitchEvent;
	StateController _stateController = StateController();

public:
	void TickComponent(float deltaTime, ELevelTick tickType, FActorComponentTickFunction* thisTickFunction) override;
	virtual TArray<StateMachineState> SwitchState(const TArray<StateMachineState>& newState);
};
