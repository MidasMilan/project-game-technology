// Fill out your copyright notice in the Description page of Project Settings.

#include "StateMachineState.h"

StateMachineState::StateMachineState(FString& stateName)
{
	_stateName = *stateName;
}

StateMachineState::~StateMachineState() = default;
