// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/// <summary>
/// This class represents a single state with all of its behaviour, events and general data.
/// It can be overriden to provide additional functional or type specification, though this is not needed for usage with the
/// state machine component
/// </summary>
class MYPROJECT_API StateMachineState
{
public:
	StateMachineState(FString& stateName);
	~StateMachineState();

	DECLARE_EVENT(FLayerViewModel, FChangedEvent)

	FString& GetName() { return _stateName; };
	FChangedEvent& OnStateEnter() { return _stateEnterEvent; };
	FChangedEvent& OnStateUpdate() { return _stateUpdateEvent; };
	FChangedEvent& OnStateExit() { return _stateExitEvent; };

protected:
	FString _stateName = "Undefined State";
	FChangedEvent _stateEnterEvent;
	FChangedEvent _stateUpdateEvent;
	FChangedEvent _stateExitEvent;
};
