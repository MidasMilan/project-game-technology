// Fill out your copyright notice in the Description page of Project Settings.
#define print(text) if(GEngine) GEngine->AddOnScreenDebugMessage(-1,30, FColor::Cyan, text)
#define printf(text, fstring)  if(GEngine) GEngine->AddOnScreenDebugMessage(-1,30, FColor::Cyan, FString::Printf(TEXT(text), fstring))

#include "TagController.h"
#include "DrawDebugHelpers.h"

ATagController::ATagController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATagController::BeginPlay()
{
	Super::BeginPlay();

	TimeBetweenRandomTargets = 500;

	ParentEnemy = Cast<AEnemyCharacter>(GetOwner());
	SetNewRandomOffset();
}

FVector ATagController::GetPlayerLocation()
{
	ACharacter* myCharacter = UGameplayStatics::GetPlayerCharacter((UObject*)GetWorld(), 0);


	//if (ParentEnemy->GetIsShooting()) {
		//return ParentEnemy->GetActorLocation();
	//}
	//else 
	if (IsValid(myCharacter)) {
		APlayerCharacter* PlayerCharacterPointer = Cast<APlayerCharacter>(myCharacter);
		if (PlayerCharacterPointer)
		{
			AActor* output = Cast<AActor>(PlayerCharacterPointer->GetTargetPoint());
			if (IsValid(output))
			{
				output->AddActorLocalTransform(FTransform(RandomOffset));
				FVector outputVector = output->GetActorLocation();
				return outputVector;
			}
		}
	}
	else 
	{
		return GetOwner()->GetActorLocation();
	}
	return myCharacter->GetActorLocation();
}

void ATagController::SetNewRandomOffset() {
	RandomOffset = FVector(TargetPositionOffset, FMath::RandRange(-TargetPositionOffset, TargetPositionOffset), 0);
}

void ATagController::GoToRandomWaypoint()
{
	MoveToLocation(GetPlayerLocation());
}

// Called every frame
void ATagController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (WalkToPlayerDistance)
	{
		GoToRandomWaypoint();
	}

	if (CurrentTimer > 0) {
		CurrentTimer -= 1;
	}

	if (CurrentTimer <= 0) {
		SetNewRandomOffset();
		CurrentTimer = TimeBetweenRandomTargets;
	}
}