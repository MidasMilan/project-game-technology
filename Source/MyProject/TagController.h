// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"
#include "MyProject/PlayerCharacter.h"
#include "MyProject/EnemyCharacter.h"
#include "TagController.generated.h"

/**
 *
 */
UCLASS()
class MYPROJECT_API ATagController : public AAIController
{
	GENERATED_BODY()
public:
	ATagController(const FObjectInitializer& ObjectInitializer);
	ATagController();
	void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	AEnemyCharacter* ParentEnemy;
	FVector RandomOffset;
	int TimeBetweenRandomTargets;
	int CurrentTimer;

private:
	UPROPERTY()
		TArray<AActor*> Waypoints;

	UFUNCTION()
		FVector GetPlayerLocation();

	UFUNCTION()
		void GoToRandomWaypoint();

	FTimerHandle TimerHandle;

	UPROPERTY(EditAnywhere)
		float TargetPositionOffset = 100;
	UPROPERTY(EditAnywhere)
		float WalkToPlayerDistance = 10000;

	void SetNewRandomOffset();
};
